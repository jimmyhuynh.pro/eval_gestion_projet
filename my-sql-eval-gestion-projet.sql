-- Creator:       MySQL Workbench 6.3.8/ExportSQLite Plugin 0.1.0
-- Author:        Jimmy
-- Caption:       New Model
-- Project:       Name of the project
-- Changed:       2021-12-20 19:05
-- Created:       2021-12-20 09:31
PRAGMA foreign_keys = OFF;

-- Schema: mydb
ATTACH "mydb.sdb" AS "mydb";
BEGIN;
CREATE TABLE "mydb"."client"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "name" VARCHAR(45),
  "adresse" VARCHAR(45)
);
CREATE TABLE "mydb"."dev"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "firstname" VARCHAR(45),
  "lastname" VARCHAR(45),
  "level" VARCHAR(45)
);
CREATE TABLE "mydb"."projet"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "title" VARCHAR(45),
  "content" VARCHAR(45),
  "client_id" INTEGER,
  CONSTRAINT "fk_projet_client1"
    FOREIGN KEY("client_id")
    REFERENCES "client"("id")
);
CREATE INDEX "mydb"."projet.fk_projet_client1_idx" ON "projet" ("client_id");
CREATE TABLE "mydb"."dev_has_projet"(
  "dev_id" INTEGER NOT NULL,
  "projet_id" INTEGER NOT NULL,
  PRIMARY KEY("dev_id","projet_id"),
  CONSTRAINT "fk_dev_has_projet_dev1"
    FOREIGN KEY("dev_id")
    REFERENCES "dev"("id"),
  CONSTRAINT "fk_dev_has_projet_projet1"
    FOREIGN KEY("projet_id")
    REFERENCES "projet"("id")
);
CREATE INDEX "mydb"."dev_has_projet.fk_dev_has_projet_projet1_idx" ON "dev_has_projet" ("projet_id");
CREATE INDEX "mydb"."dev_has_projet.fk_dev_has_projet_dev1_idx" ON "dev_has_projet" ("dev_id");
CREATE TABLE "mydb"."developper_has_projet"(
  "developper_id" INTEGER NOT NULL,
  "projet_id" INTEGER NOT NULL,
  PRIMARY KEY("developper_id","projet_id"),
  CONSTRAINT "fk_Developper_has_Projet_Developper"
    FOREIGN KEY("developper_id")
    REFERENCES "dev"("id"),
  CONSTRAINT "fk_Developper_has_Projet_Projet1"
    FOREIGN KEY("projet_id")
    REFERENCES "projet"("id")
);
CREATE INDEX "mydb"."developper_has_projet.fk_Developper_has_Projet_Projet1_idx" ON "developper_has_projet" ("projet_id");
CREATE INDEX "mydb"."developper_has_projet.fk_Developper_has_Projet_Developper_idx" ON "developper_has_projet" ("developper_id");
COMMIT;
