import { Request, Response } from "express-serve-static-core";

export default class HomeController {
    
    // Affiche la liste des projets

    static index(req: Request, res: Response) {
        const db = req.app.locals.db;

        const projets = db.prepare('SELECT * FROM projet').all();

        res.render('pages/index', {
            title: 'Gestion de projets',
            projets: projets
        });
    }
    
}