import { Request, Response } from "express-serve-static-core";

export default class ProjetController {
    
    // Affiche la liste des projets
    
    static index(req: Request, res: Response) {
        const db = req.app.locals.db;

        const projets = db.prepare('SELECT * FROM projet').all();

        res.render('pages/index', {
            title: 'Projets',
            projets: projets
        });
    }

    // Affiche le formulaire de creation de projet

    static showForm(req: Request, res: Response): void {
        
        res.render('pages/projet-create');
    }

    // Recupere le formulaire et insere le projet en database

    static create(req: Request, res: Response): void {
        const db = req.app.locals.db;
    
        const client = db.prepare('SELECT * FROM client WHERE id = ?').all(req.params.id);
        
        db.prepare('INSERT INTO projet ("title", "content", "client_id") VALUES (?, ?, ?)').run(req.body.title, req.body.content, 1);
        
        ProjetController.index(req, res);
        res.render('pages/projet-create', {
            projet: client
        });
    }

    
    // Affiche un projet
    
    static read(req: Request, res: Response): void {
        const db = req.app.locals.db;

        const projets = db.prepare('SELECT * FROM projet WHERE id = ?').get(req.params.id);

        res.render('pages/projet', {
            projet: projets
        });
    }

    // Affiche le formulaire pour modifier un projet
    
    static showFormUpdate(req: Request, res: Response) {
        const db = req.app.locals.db;

        const projets = db.prepare('SELECT * FROM projet WHERE id = ?').get(req.params.id);
        
        res.render('pages/projet-update', {
            projet: projets
        });
    }

    // Recupere le formulaire du projet modifié et l'ajoute à la database
    
    static update(req: Request, res: Response) {
        const db = req.app.locals.db;

        db.prepare('UPDATE projet SET title = ?, content = ? WHERE id = ?').run(req.body.title, req.body.content, req.params.id);

        ProjetController.index(req, res);
    }

    
    // Supprimer un projet
    
    static delete(req: Request, res: Response) {
        const db = req.app.locals.db;

        db.prepare('DELETE FROM projet WHERE id = ?').run(req.params.id);

        ProjetController.index(req, res);
    }

}

